--
-- Creating structure and importing data
--
DROP DATABASE IF EXISTS inkitt;
CREATE DATABASE inkitt;

USE inkitt;

-- Db structure for reading

CREATE TABLE `reading` (
  `is_app_event` varchar(5) DEFAULT NULL,
  `visitor_id` varchar(36) DEFAULT NULL,
  `id` varchar(36) DEFAULT NULL,
  `visit_id` varchar(36) DEFAULT NULL,
  `tracking_time` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `story_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Db structure for stories
CREATE TABLE `stories` (
  `id` int(8) DEFAULT NULL,
  `user_id` int(8) DEFAULT NULL,
  `teaser` varchar(512) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `cover` varchar(100) DEFAULT NULL,
  `category_one` varchar(30) DEFAULT NULL,
  `category_two` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Db structure for visits
CREATE TABLE `visits` (
  `visitor_id` varchar(36) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `country` varchar(27) DEFAULT NULL,
  `timezone` varchar(27) DEFAULT NULL,
  `location_accuracy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- import csv files in db
LOAD DATA INFILE '/Users/techkriti/PycharmProjects/codility/inkitt/test_work_r04/reading.csv'
INTO TABLE reading FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

LOAD DATA INFILE '/Users/techkriti/PycharmProjects/codility/inkitt/test_work_r04/stories.csv'
INTO TABLE stories FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

LOAD DATA INFILE '/Users/techkriti/PycharmProjects/codility/inkitt/test_work_r04/visits.csv'
INTO TABLE visits FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

-- Task 1
-- Per day count of readers including anonymous visits or multiple visits
select date(created_at), count(*) from reading r join stories s on r.story_id=s.id where (s.category_one='horror' or s.category_two='horror') group by date(r.created_at);

-- Per day count of readers with multiple visits
select date(created_at), count(*) from reading r join stories s on r.story_id=s.id where (category_one='horror' or category_two='horror') and r.user_id is not null group by date(created_at);

-- Task 1.1
-- I do not understand the meaning of "how much did they read", Is it about progress?

-- Task 1.2
select date(created_at), count(distinct r.user_id) from reading r join stories s on r.story_id=s.id where (category_one='horror' or category_two='horror') and r.user_id is not null group by date(created_at);

-- Task 1.3
select date(created_at), count(distinct r.user_id), v.country from reading r join stories s on (r.story_id=s.id) join visits v on r.visit_id=v.visitor_id  where (category_one='horror' or category_two='horror') and r.user_id is not null group by date(created_at), country;



