# README #

This is my solution for inkitt challenge

### What is this repository for? ###

This is my solution for inkitt challenge

### How do I get set up? ###

* task1 - The solution is in task1.sql (I did not understand one question - How much did they read ?)
* task 2 - task2.ipynb
* task3  - task3.txt (My understanding was - this is a subjective question and I've choose to answer this in text). Please let me know in case, It was expected to deliver a solution in code for this problem.

### Dependencies ###

* Python 3 environment
* Pandas and numpy libraries
